import { Injectable } from '@angular/core';
import { User } from '../model/user';
import { HttpClient } from '@angular/common/http';
import { Response } from '../model/response';
import { PersonList } from '../model/person-list';
import { Person } from '../model/person';
import { SignUp } from '../model/sign-up';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private _httpClient: HttpClient) { }

  private LOGIN_API: string = 'http://localhost:8080/login/login';
  private SIGN_UP: string = 'http://localhost:8080/login/sinUp';
  private GET_PERSON_LIST = 'http://localhost:8080/person/list';
  private DELETE_USER = 'http://localhost:8080/person/deleteById/';
  private ADD_USER = 'http://localhost:8080/person/save';
  private UPDATE_USER = 'http://localhost:8080/person/update'

  public login(user: User) {
    return this._httpClient.post<Response>(this.LOGIN_API, user);
  }

  public signUp(user: SignUp) {
    return this._httpClient.post<Response>(this.SIGN_UP, user);
  }

  public saveUser(person: Person) {
    return this._httpClient.post<Response>(this.ADD_USER, person);
  }

  public getPersonList() {
    return this._httpClient.get<Response>(this.GET_PERSON_LIST);
  }

  public deleteUser(id: string) {
    return this._httpClient.delete<Response>(this.DELETE_USER + id);
  }

  public updateUser(person: PersonList) {
    return this._httpClient.put<Response>(this.UPDATE_USER, person);
  }
}
