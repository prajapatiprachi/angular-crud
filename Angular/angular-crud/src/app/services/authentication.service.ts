import { Injectable } from '@angular/core';
import { Person } from '../model/person';
import { Router } from '@angular/router';
import { HeaderComponent } from '../header/header.component';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  constructor(private _router: Router) { 
    
  }

  public createSession(user: Person) {
    sessionStorage.setItem("userEmail", user.email);
    sessionStorage.setItem("name", user.name);
  }

  public isUserExist() {
    return !(sessionStorage.getItem('userEmail') === null)
  }

  public canActivate() {
    if (this.isUserExist()) {
      return true;
    } else {
      this._router.navigate(['/login']);
      return false;
    }
  }

  public logout() {
    sessionStorage.removeItem("userEmail");
    sessionStorage.removeItem("name");
  }
}
