import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Person } from 'src/app/model/person';
import { PersonList } from 'src/app/model/person-list';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-add-user',
  templateUrl: './add-user.component.html',
  styleUrls: ['./add-user.component.css']
})
export class AddUserComponent implements OnInit {

  userName: string = '';
  userAge: string = '';
  userAddress: string = '';
  userEmail: string = '';
  userContact: string = '';
  user: Person = new Person();
  isNewUser: boolean = true;
  updateUser: PersonList = new PersonList();

  constructor(private route: Router, private _userService: UserService, private _routerLink: ActivatedRoute) {
    this._routerLink.params.subscribe(param => {
      if(param.employee) {
        this.updateUser = JSON.parse(param.employee) as PersonList;
        this.user.name = this.updateUser.name;
        this.user.age = this.updateUser.age;
        this.user.address = this.updateUser.address;
        this.user.contact = this.updateUser.contact;
        this.user.email = this.updateUser.email;
        this.isNewUser = false;
      }
    });
  }

  ngOnInit(): void {
    
  }

  public saveUserHandler() {
    this._userService.saveUser(this.user).subscribe(
      (data) => {
        if (data.status) {
          this.route.navigate(['/welcome']);
        } else {
          alert("Error");
        }
      }
    );
  }

  public updateUserHandler() {
    this.updateUser.name = this.user.name;
    this.updateUser.age = this.user.age;
    this.updateUser.address = this.user.address;
    this.updateUser.contact = this.user.contact;
    this.updateUser.email = this.user.email;
    this._userService.updateUser(this.updateUser).subscribe(
      (response) => {
        if (response.status) {
          this.route.navigate(['/welcome']);
        } else {
          alert("Error");
        }
      }
    );
  }

}
