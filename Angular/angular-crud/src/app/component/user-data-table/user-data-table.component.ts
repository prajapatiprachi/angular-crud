import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { PersonList } from 'src/app/model/person-list';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-user-data-table',
  templateUrl: './user-data-table.component.html',
  styleUrls: ['./user-data-table.component.css']
})
export class UserDataTableComponent implements OnInit {

  public personList: Array<PersonList> = [];

  constructor(private _userService: UserService, private route: Router) { }

  ngOnInit(): void {
    this._userService.getPersonList().subscribe(
      (response) => {
        this.personList = response.data;
      }
    );
  }

  public deleteEmployee(id: string) {
    this._userService.deleteUser(id).subscribe(
      (data) => {
        this.ngOnInit();
      }
    );
  }

  public addNewUserHandler(operationType: string) {
    this.route.navigate(['sinup', operationType]);
  }

}
