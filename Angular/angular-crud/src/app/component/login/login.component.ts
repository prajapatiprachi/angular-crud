import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { User } from 'src/app/model/user';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  public userName: string = '';
  public userPassword: string = '';
  public user: User = new User();
  public error_message: string = '';
  public isErrorOccured: boolean = false;

  constructor(private route: Router, private _userService: UserService, private _authenticateUser: AuthenticationService) { }

  ngOnInit(): void {
  }

  public loginHandler() {
    this._userService.login(this.user).subscribe(
      (response) => {
        if (response.status) {
          this._authenticateUser.createSession(response.data);
          this.route.navigate(['/welcome']);
        } else {
          this.isErrorOccured = true;
          this.error_message = 'User Not Found';
        }
      }
    );
  }

  public signUpHandler(operationType:string) {
    this.route.navigate(['sinup', operationType]);
  }

}
