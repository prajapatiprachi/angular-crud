import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { SignUp } from 'src/app/model/sign-up';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-sinup',
  templateUrl: './sinup.component.html',
  styleUrls: ['./sinup.component.css']
})
export class SinupComponent implements OnInit {

  userName: string = '';
  userAge: string = '';
  userAddress: string = '';
  userEmail: string = '';
  userContact: string = '';
  password: string = '';
  confirmPassword: string = '';
  user: SignUp = new SignUp();
  operationType: string | null = '';

  constructor(private route: Router, private _activateRoute: ActivatedRoute, private _userService: UserService, private _authenticateUser: AuthenticationService) {
    this.operationType = this._activateRoute.snapshot.paramMap.get("operation");
  }

  ngOnInit(): void {
  }

  public signupHandler() {
    let responseData;
    if (this.operationType == 'New User') {
      responseData = this._userService.signUp(this.user);
    } else {
      responseData = this._userService.saveUser(this.user);
    }
    responseData.subscribe(
      (response) => {
        if (response.status) {
          console.log(response);
          let responseMessage = response.msg;
          alert(responseMessage)
          if (responseMessage == "Add user successfully.") {
            this.route.navigate(['/welcome']);
          } else {
            this._authenticateUser.createSession(response.data);
            this.route.navigate(['/login']);
          }
        } else {
          alert(response.msg);
        }
      }
    );
  }

}
