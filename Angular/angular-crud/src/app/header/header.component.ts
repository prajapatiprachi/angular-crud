import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthenticationService } from '../services/authentication.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  public user: any = sessionStorage.getItem("name");;

  constructor(public _authenticateUser: AuthenticationService, private route: Router) { }

  ngOnInit(): void { }

  public LogoutHandler() {
    this._authenticateUser.logout();
    this.route.navigate(["/login"]);
  }

}
