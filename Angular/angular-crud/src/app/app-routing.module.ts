import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AddUserComponent } from './component/add-user/add-user.component';
import { LoginComponent } from './component/login/login.component';
import { SinupComponent } from './component/sinup/sinup.component';
import { WelcomeComponent } from './component/welcome/welcome.component'
import { AuthenticationService } from './services/authentication.service';

const routes: Routes = [
  { path: '', component: LoginComponent},
  { path: 'login', component: LoginComponent},
  { path: 'welcome', component: WelcomeComponent, canActivate: [AuthenticationService] },
  { path: 'new_user', component: AddUserComponent, canActivate: [AuthenticationService] },
  { path: 'person/update/:employee', component: AddUserComponent, canActivate: [AuthenticationService] },
  { path: 'sinup/:operation', component: SinupComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
