import { User } from './user'

export class Response {

    msg: string = '';
    status: boolean = true;
    data: any;

    constructor() {

    }
}
