export class PersonList {

    id: string = '';
    name: string = '';
    age: string = '';
    address: string = '';
    email: string = '';
    contact: string = '';
}
