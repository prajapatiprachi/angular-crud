package example.micronaut.vo

import example.micronaut.domain.Employee
import example.micronaut.domain.EmployeeRole
import example.micronaut.myUtils.Helper


class EmployeeVO {

    Long age
    Float salary
    String address
    String joiningDate
    String firstName
    String lastName
    Long phoneNo
    String dob
    EmployeeRole role
    Long companyId

    EmployeeVO(Employee employee) {
        this.age = employee.age
        this.salary = employee.salary
        this.address = employee.address
        this.joiningDate = Helper.parseDateString( employee.joiningDate)
        this.firstName = employee.firstName
        this.lastName = employee.lastName
        this.phoneNo = employee.phoneNo
        this.dob = Helper.parseDateString(employee.dob)
        this.role= employee.role
        this.companyId= employee.companyId

    }
}
