package example.micronaut.domain

import grails.gorm.annotation.Entity
import org.grails.datastore.gorm.GormEntity

@Entity
class Company implements GormEntity<Company> {

    String name
    String branch
    Long phoneNo
    String email
    String address


    static hasMany = [employee: Employee]

    static constraints = {
        branch nullable: true
        phoneNo nullable: true
        email nullable: true
    }

}
