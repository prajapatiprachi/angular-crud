package example.micronaut.domain

import org.grails.datastore.gorm.GormEntity

import grails.gorm.annotation.Entity
import org.hibernate.FetchMode

/*@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Integer id*/

@Entity
class Employee implements GormEntity<Employee> {

    String firstName
    String lastName
    Date dob
    Long age
    String address
    Date joiningDate
    Float salary
    Long phoneNo
    EmployeeRole role


    static belongsTo = [company: Company]

    static hasMany = [attendence: Attendance]

    static mapping = {
        role fetch: FetchMode.JOIN
        company fetch: FetchMode.JOIN
    }

}
