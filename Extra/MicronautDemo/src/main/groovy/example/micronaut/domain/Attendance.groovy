package example.micronaut.domain

import grails.gorm.annotation.Entity
import org.grails.datastore.gorm.GormEntity

@Entity
class Attendance implements GormEntity<Attendance> {

    Date createdDate
    Date checkIn
    Date checkOut
    AttendanceStatus status

    static belongsTo = [employee: Employee]



}
