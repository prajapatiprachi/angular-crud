package example.micronaut.domain

import grails.gorm.annotation.Entity
import org.grails.datastore.gorm.GormEntity

@Entity
class EmployeeRole implements GormEntity<EmployeeRole> {

    String authority
    String description

    static constraints = {
        authority unique: true
    }

}
