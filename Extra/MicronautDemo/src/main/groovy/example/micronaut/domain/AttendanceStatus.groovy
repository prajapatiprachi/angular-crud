package example.micronaut.domain

enum AttendanceStatus {

    PRESENT("Present"),
    ABSENT("Absent"),
    LEAVE("Leave"),

    private String name

    AttendanceStatus(String name){
        this.name = name
    }

    String getName(){
        return name
    }


}