package example.micronaut.controller


import example.micronaut.co.EmployeeRoleCO
import example.micronaut.domain.EmployeeRole
import example.micronaut.service.EmployeeRoleService
import io.micronaut.http.HttpResponse
import io.micronaut.http.MediaType
import io.micronaut.http.annotation.Body
import io.micronaut.http.annotation.Controller
import io.micronaut.http.annotation.Get
import io.micronaut.http.annotation.Post
import io.micronaut.views.View

import javax.inject.Inject


@Controller("/employeeRole")
class EmployeeRoleController {

    @Inject
    EmployeeRoleService employeeRoleService


    @View("addRole")
    @Get("/add")
    String addEmployeeRole() {
        return "addRole"
    }

    @View("/roleList")
    @Post(value = "/save", consumes = MediaType.APPLICATION_FORM_URLENCODED)
    HttpResponse save(@Body EmployeeRoleCO employeeRoleCO) {
        employeeRoleService.saveEmployeeRoleData(employeeRoleCO)
        HttpResponse.seeOther(URI.create("/employeeRole/show"))
    }

    @View("/roleList")
    @Get("/show")
    HttpResponse view() {
        List<EmployeeRole> employeeRole = employeeRoleService.fetchEmployeeRoleList()
        HttpResponse.ok([employeeRole: employeeRole])
    }


    @View("/updateRole")
    @Get("/edit/{id}")
    HttpResponse edit(Long id) {
        HttpResponse.ok([employeeRole: employeeRoleService.findEmployeeRoleById(id)])

    }

    @Get("delete/{id}")
    HttpResponse delete(Long id) {
        employeeRoleService.deleteById(id)
        HttpResponse.seeOther(URI.create("/employeeRole/show"))
    }

}
