package example.micronaut.controller

import example.micronaut.co.CompanyCO
import example.micronaut.co.EmployeeCO
import example.micronaut.co.EmployeeListCO
import example.micronaut.domain.Company
import example.micronaut.domain.Employee
import example.micronaut.domain.EmployeeRole
import example.micronaut.service.CompanyService
import example.micronaut.service.EmployeeRoleService
import example.micronaut.service.EmployeeService
import example.micronaut.vo.EmployeeVO
import io.micronaut.http.MediaType
import io.micronaut.http.annotation.Body
import io.micronaut.http.annotation.Controller
import io.micronaut.http.annotation.Get
import io.micronaut.http.annotation.Post
import io.micronaut.views.View
import io.micronaut.http.HttpResponse

import javax.annotation.Nullable
import javax.inject.Inject
import javax.validation.constraints.Null


@Controller("/employee")
class EmployeeController {

    @Inject
    EmployeeService employeeService

    @Inject
    CompanyService companyService
    @Inject
    EmployeeRoleService employeeRoleService

    @View("/addEmployee")
    @Get("/add")
    HttpResponse add() {
        List<Company> companyList = companyService.findAll()
        List<EmployeeRole> employeeRoleList = employeeRoleService.fetchEmployeeRoleList()
        HttpResponse.ok(['companyList': companyList, 'employeeRoleList': employeeRoleList])
    }

    @View("/employeeList")
    @Post(value = "/save", consumes = MediaType.APPLICATION_FORM_URLENCODED)
    HttpResponse save(@Body EmployeeCO employeeCO) {
        employeeService.save(employeeCO)
        HttpResponse.seeOther(URI.create("/employee/list"))
    }

    @View("/employeeList")
    @Get("/list")
    HttpResponse list(@Nullable Long companyId , @Nullable String search ,@Nullable String fromDate ,@Nullable String toDate) {
        List<Employee> employees = employeeService.fetchEmployeeList(companyId, search ,fromDate ,toDate )
        HttpResponse.ok(['employee' : employees ,'company' : companyService.findAll() ])
    }

    @View("/updateEmployee")
    @Get("/edit/{id}")
    HttpResponse edit(Long id) {
        List<Company> companyList = companyService.findAll()
        List<EmployeeRole> employeeRoleList = employeeRoleService.fetchEmployeeRoleList()
        HttpResponse.ok(['employee': employeeService.findById(id) ,'companyList': companyList, 'employeeRoleList': employeeRoleList])
    }

   @View("/employeeList")
   @Post(value="update" , consumes = MediaType.APPLICATION_FORM_URLENCODED)
   HttpResponse update(@Body EmployeeCO employeeCO) {
       employeeService.update(employeeCO)
       HttpResponse.seeOther(URI.create("/employee/list"))
   }

    @Get("delete/{id}")
    HttpResponse delete(Long id) {
        employeeService.delete(id)
        HttpResponse.seeOther(URI.create("/employee/list"))
    }


}
