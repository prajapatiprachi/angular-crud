package example.micronaut.controller

import io.micronaut.http.annotation.Controller
import io.micronaut.http.annotation.Get
import io.micronaut.views.View


@Controller("/")
class HomeController {

    @View("/welcomePage")
    @Get("/")
    String index() {
        return "welcomePage"
    }
}
