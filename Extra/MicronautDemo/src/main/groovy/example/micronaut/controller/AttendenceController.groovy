package example.micronaut.controller

import example.micronaut.co.AttendenceCO
import example.micronaut.domain.Attendance
import example.micronaut.service.AttendanceService
import io.micronaut.http.HttpResponse
import io.micronaut.http.MediaType
import io.micronaut.http.annotation.Body
import io.micronaut.http.annotation.Controller
import io.micronaut.http.annotation.Get
import io.micronaut.http.annotation.Post
import io.micronaut.views.View

import javax.annotation.Nullable
import javax.inject.Inject


@Controller("/attendence")
class AttendenceController {

    @Inject
    AttendanceService attendenceService


    /*@View("/addAttendenceData")
    @Get("/add")
    String add() {
        return "addAttendenceData"
    }
*/
   /* @View("/submit")
    @Post(value = "/save", consumes = MediaType.APPLICATION_FORM_URLENCODED)
    String saveData(@Body AttendenceCO attendenceCO) {
        attendenceService.saveAttendenceData(attendenceCO)
        return "submit"
    }*/

    @View("/attendanceList")
    @Get("/list")
    HttpResponse list(@Nullable Long companyId) {
        List<Map> attendenceList = attendenceService.fetchEmployeeAttendance(companyId)
        HttpResponse.ok([attendence: attendenceList])

    }

   /* @View("/updateAttendenceData")
    @Get("/edit/{id}")
    HttpResponse edit(Long id) {
        HttpResponse.ok([attendence: attendenceService.findAttendenceById(id)])

    }
    */


}
