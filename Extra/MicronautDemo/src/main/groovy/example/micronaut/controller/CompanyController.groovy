package example.micronaut.controller

import example.micronaut.co.CompanyCO
import example.micronaut.domain.Company
import example.micronaut.service.CompanyService
import io.micronaut.http.HttpResponse
import io.micronaut.http.MediaType
import io.micronaut.http.annotation.Body
import io.micronaut.http.annotation.Controller
import io.micronaut.http.annotation.Get
import io.micronaut.http.annotation.Post
import io.micronaut.views.View

import javax.inject.Inject


@Controller("/company")
class CompanyController {

    @Inject
    CompanyService companyService

    @View("/addCompany")
    @Get("/add")
    String saveData() {
        return "addCompany"
    }

    @View("/companyList")
    @Post(value = "/save", consumes = MediaType.APPLICATION_FORM_URLENCODED)
    HttpResponse save(@Body CompanyCO companyCO) {
        companyService.saveCompanyData(companyCO)
        HttpResponse.seeOther(URI.create("/company/show"))
    }

    @View("/companyList")
    @Get("/show")
    HttpResponse view() {
        List<Company> companyList = companyService.findAll()
        HttpResponse.ok([company: companyList])

    }

    @View("/updateCompany")
    @Get("/edit/{id}")
    HttpResponse edit(Long id) {
        HttpResponse.ok([company: companyService.findById(id)])

    }

    @Get("delete/{id}")
    HttpResponse delete(Long id) {
        companyService.deleteById(id)
        HttpResponse.seeOther(URI.create("/company/show"))
    }
}
