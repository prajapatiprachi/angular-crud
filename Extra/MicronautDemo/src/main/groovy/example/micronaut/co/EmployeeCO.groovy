package example.micronaut.co

import example.micronaut.domain.EmployeeRole

import javax.validation.constraints.NotEmpty
import javax.validation.constraints.NotNull


class EmployeeCO {

    Long employeeId  //used when update employee

    Long age

    Float salary

    String address

    String joiningDate

    @NotEmpty
    String firstName

    @NotNull
    Long companyId

    String lastName

    Long phoneNo

    String dob

    @NotNull
   String role

    String search



}
