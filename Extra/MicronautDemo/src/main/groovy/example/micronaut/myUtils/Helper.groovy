package example.micronaut.myUtils

import java.text.SimpleDateFormat
import java.util.regex.Pattern

class Helper {

    static final String DATE_FORMAT = "dd/MM/yyyy"
    static SimpleDateFormat simpleDateFormat = new SimpleDateFormat(DATE_FORMAT)

    static isNumberOnly(String str){

        char[] ch = str.toCharArray()
        for(char c : ch) {
             if(Character.isDigit(c)) {
                 return true
             }
            return false
        }
    }

    static Date parseDateString(String date) {
         return date ? simpleDateFormat.parse(date) : null
    }

    static String parseDateString(Date date) {
        return date ? simpleDateFormat.format(date) : null
    }

}
