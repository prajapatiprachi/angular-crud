package example.micronaut.service

import example.micronaut.co.AttendenceCO
import example.micronaut.domain.Attendance
import example.micronaut.domain.Company
import example.micronaut.domain.Employee
import grails.gorm.services.Service
import grails.gorm.transactions.Transactional

import javax.inject.Inject
import java.time.LocalDate


@Service(Attendance)
abstract class AttendanceService {

    @Inject
    CompanyService companyService

    @Inject
    EmployeeService employeeService

    @Transactional
    List<Map> fetchEmployeeAttendance(Long companyId) {
        List<Map> employeeAttendanceList = []
        Company company =companyService.findById(companyId)
        List<Employee> employeeList = companyId ? employeeService.fetchEmployeeListByCompany(company.id) : employeeService.findAll()
        employeeList.each { employee ->
            Attendance attendance = fetchAttendanceDetailsForToday(employee)
            employeeAttendanceList << ['attendanceId': attendance?.id, 'name': employee.firstName, 'status': attendance?.status, 'checkInTime': attendance?.checkIn, 'checkOutTime': attendance?.checkOut]
        }
        return employeeAttendanceList
    }

    private Attendance fetchAttendanceDetailsForToday(Employee employee) {
        return Attendance.createCriteria().get {

        }
    }

}
