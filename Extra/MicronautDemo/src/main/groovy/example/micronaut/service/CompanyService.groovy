package example.micronaut.service

import example.micronaut.co.CompanyCO
import example.micronaut.co.EmployeeCO
import example.micronaut.domain.Company
import example.micronaut.domain.Employee
import example.micronaut.vo.EmployeeVO
import grails.gorm.services.Service
import grails.gorm.transactions.Transactional


@Service(Company)
abstract class CompanyService {

    abstract Company findById(Long id)

    abstract List<Company> findAll()

    @Transactional
    void saveCompanyData(CompanyCO companyCO) {
        Company company = new Company(
                name: companyCO.name,
                branch: companyCO.branch,
                phoneNo: companyCO.phoneNo,
                email: companyCO.email,
                address: companyCO.address
        )
        company.save()
    }

    @Transactional
    void deleteById(Long companyId) {
        Company company = findById(companyId)
        company.delete()
    }


}
