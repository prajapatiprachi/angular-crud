package example.micronaut.service

import example.micronaut.co.CompanyCO
import example.micronaut.co.EmployeeCO
import example.micronaut.co.EmployeeRoleCO
import example.micronaut.domain.Attendance
import example.micronaut.domain.Company
import example.micronaut.domain.Employee
import example.micronaut.domain.EmployeeRole
import example.micronaut.myUtils.Helper;
import example.micronaut.vo.EmployeeVO
import grails.gorm.services.Service
import grails.gorm.transactions.Transactional
import org.hibernate.criterion.Projections

import javax.inject.Inject


@Service(Employee)
abstract class EmployeeService {

    @Inject
    CompanyService companyService

    abstract Employee findById(Long id)

    abstract List<Employee> findAll()

    @Transactional
    void save(EmployeeCO employeeCO) {
        Company company = employeeCO.companyId ? Company.findById(employeeCO.companyId) : null
        EmployeeRole employeeRole = employeeCO.role ? EmployeeRole.findByAuthority(employeeCO.role) : null
        if (!employeeRole) {
            print("Employee role not found")
        } else if (!company) {
            print("Company not found")
        } else {
            Employee employee = new Employee(
                    firstName: employeeCO.firstName,
                    lastName: employeeCO.lastName,
                    dob: Helper.parseDateString(employeeCO.dob),
                    age: employeeCO.age,
                    address: employeeCO.address,
                    joiningDate: Helper.parseDateString(employeeCO.joiningDate),
                    salary: employeeCO.salary,
                    phoneNo: employeeCO.phoneNo,
                    role: employeeRole,
                    company: company
            )
            employee.save(failOnError: true)
        }
    }


    List<Employee> fetchEmployeeListByCompany(Long companyId) {
        return Employee.createCriteria().listDistinct {
            companyId ? eq('company', companyService.findById(companyId)) : ''
        } as List<Employee>
    }

    @Transactional
    List<Employee> fetchEmployeeList(Long companyId, String search , String fromDate , String toDate) {

        return Employee.createCriteria().list {
            companyId ? 'company' { idEq(companyId) } : ''
            or {
                search ? ilike('firstName', "%${search.trim()}%") : ''
                search ? ilike('lastName', "%${search.trim()}%") : ''
                search ? ilike('address', "%${search.trim()}%") : ''
                search && Helper.isNumberOnly(search.trim()) ? eq('phoneNo', search.trim().toLong()) : ''
                search ? ilike('address', "%${search.trim()}%") : ''
                search && Helper.isNumberOnly(search.trim()) ? eq('age', search.trim().toLong()) : ''
               fromDate && toDate ?  between('joiningDate', Helper.parseDateString(fromDate), Helper.parseDateString(toDate)) : ''
                'company' {
                    search && Helper.isNumberOnly(search.trim()) ? idEq(search.trim().toLong()) : ''
                }
                'role' {
                    search ? ilike('authority', "%${search.trim()}%") : ''
                }
            }
        } as List<Employee>
    }


    @Transactional
    void update(EmployeeCO employeeCO) {
        Company company = employeeCO.companyId ? Company.findById(employeeCO.companyId) : null
        EmployeeRole employeeRole = employeeCO.role ? EmployeeRole.findByAuthority(employeeCO.role) : null
        if (!company) {
            print("company not found")
        } else if (!employeeRole) {
            print("role not found")
        } else {
            Employee employee = Employee.findById(employeeCO.employeeId)
            employee.firstName = employeeCO.firstName
            employee.lastName = employeeCO.lastName
            employee.age = employeeCO.age
            employee.address = employeeCO.address
            employee.salary = employeeCO.salary
            employee.joiningDate = Helper.parseDateString(employeeCO.joiningDate)
            employee.phoneNo = employeeCO.phoneNo
            employee.dob = Helper.parseDateString(employeeCO.dob)
            employee.role = employeeRole
            employee.company = company
            employee.save()
        }
    }

    @Transactional
    void delete(Long employeeId) {
        Employee employee = findById(employeeId)
        employee.delete()
    }


}



