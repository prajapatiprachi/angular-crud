package example.micronaut.service

import example.micronaut.co.EmployeeRoleCO
import example.micronaut.domain.Employee
import example.micronaut.domain.EmployeeRole

import grails.gorm.services.Service
import grails.gorm.transactions.Transactional

import javax.inject.Inject


@Service(EmployeeRole)
abstract class EmployeeRoleService {



    @Transactional
    void saveEmployeeRoleData(EmployeeRoleCO employeeRoleCO) {
         EmployeeRole employeeRole=new EmployeeRole(
                 authority:employeeRoleCO.authority,
                 description: employeeRoleCO.description,

         )
       employeeRole.save(FailOnError: true)
    }

    @Transactional
    List<EmployeeRole> fetchEmployeeRoleList() {
       return EmployeeRole.findAll()
    }

    @Transactional
     EmployeeRole findEmployeeRoleById(Long id) {
        return EmployeeRole.findById(id)
    }
    @Transactional
    void deleteById(Long employeeRoleId) {
        EmployeeRole employeeRole1 = EmployeeRole.findById(employeeRoleId)
        employeeRole1.delete()
    }

}
