package com.example.demo.controller

import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod


@Controller
class IndexController {


    @RequestMapping("/")
    String getFirst(){
        return "index"
    }


}
