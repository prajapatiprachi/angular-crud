package com.example.demo.service

import com.example.demo.Repository.EmployeeRepository
import com.example.demo.entity.Employee
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service("Employee")
public class EmployeeServiceImpl implements EmployeeService {

    @Autowired
    EmployeeRepository employeeRepository


    @Override
    Employee saveEmployee(Employee employee) {
        return employeeRepository.save(employee)
    }


    @Override
    void deleteData(Integer id) {
        employeeRepository.deleteById(id)

    }

    @Override
    Employee getEmployeeById(Integer id) {
        return employeeRepository.getOne(id)
    }



    @Override
    List<Employee> listAllEmployee() {
        return employeeRepository.findAll();
    }


}
