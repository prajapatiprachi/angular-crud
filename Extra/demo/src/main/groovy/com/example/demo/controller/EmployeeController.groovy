package com.example.demo.controller

import com.example.demo.service.EmployeeService
import com.example.demo.entity.Employee
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod


@Controller()
class EmployeeController {

    @Autowired
    EmployeeService employeeService


    @PostMapping("/employee/add")
    String addEmployee(Employee employee) {
        employeeService.saveEmployee(employee)
        return "submit"
    }

    @RequestMapping(value = "/form", method = RequestMethod.GET)
    String getEmployee() {
        return "saveEmployee"
    }

    @RequestMapping(value = "/employee/show", method = RequestMethod.GET)
    String list(Model model) {
        List<Employee> employeeList = employeeService.listAllEmployee()
        model.addAttribute(employeeList)
        return "employeeShow"
    }
    @RequestMapping("employee/edit/{id}")
    public String edit(@PathVariable Integer id, Model model) {
        model.addAttribute("employee",employeeService.getEmployeeById(id))
        return "updateEmployee"
    }

    @GetMapping("employee/delete/{id}")
    public String delete(@PathVariable Integer id,Model model) {
        employeeService.deleteData(id)
        List<Employee> employeeList = employeeService.listAllEmployee()
        model.addAttribute(employeeList)
        return "employeeShow"
    }

    @GetMapping("employee/view/{id}")
    public String view(@PathVariable Integer id,Model model) {
       // Employee employees = employeeService.getEmployeeById(id)
        model.addAttribute("employees",employeeService.getEmployeeById(id))
        //model.addAttribute(employees)
        return "viewEmployee"
    }



}
