package com.example.demo.service

import com.example.demo.entity.Employee


public interface EmployeeService {

    List<Employee> listAllEmployee()

    Employee saveEmployee(Employee employee)

    void deleteData(Integer id)

    Employee  getEmployeeById(Integer id)



}